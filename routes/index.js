var express = require("express");
var router = express.Router();

var indexController = require("../controllers/index-controller");
// var chatController = require("../controllers/chat-controller");

/* GET home page and products list */
router.get("/", indexController.get_list_product);

// create chat session route
router.get("/createChatSession", indexController.create_chat_session);

// handle search action
router.get("/search", indexController.search_product);

module.exports = router;