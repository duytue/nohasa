var express = require("express");
var router = express.Router();

var productController = require("../controllers/products-controller")


router.get("/", function (req, res) {
  res.redirect("/");
});

router.get("/:altName", productController.getProductByAltName);

module.exports = router;