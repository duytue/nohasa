module.exports = {
  production: {
    API_URL: "https://api.nohasa.net/",
    SECRET_KEY: "7d0d383d-cb35-41b6-aa1b-c282054668c5",
    CACHE_TIMEOUT: 30
  },
  development: {
    API_URL: "http://localhost:3001/",
    SECRET_KEY: "9fc83403-e2cb-4053-8328-28945511fddf",
    CACHE_TIMEOUT: 10
  }
};