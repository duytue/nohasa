function showDescription(product) {
  var description = document.getElementById(product)
  description.style.visibility = "visible";
}

function hideDescription(product) {
  var description = document.getElementById(product)
  description.style.visibility = "hidden";
}

function addToCart(productId, name) {

  console.log("Product id " + productId + " is selected");

  $.notify({
    // options
    message: name + ' added to cart'
  }, {
    // settings
    element: 'body',
    type: 'success',
    allow_dismiss: true,
    placement: {
      from: "top",
      align: "center"
    },
    template: '<div data-notify="container" class="col-11 col-sm-3 alert alert-{0}" role="alert">' +
      '<span data-notify="icon"></span> ' +
      '<span data-notify="title">{1}</span> ' +
      '<span data-notify="message">{2}</span>' +
      '<div class="progress" data-notify="progressbar">' +
      '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
      '</div>' +
      '<a href="{3}" target="{4}" data-notify="url"></a>' +
      '</div>'
  });
}

function search(e) {
  e.preventDefault();
  $.ajax({
    url: "/search",
    type: "GET",
    contentType: "application/json",
    data: JSON.stringify({
      searchQuery: $("#search").val()
    }),
    success: function (data) {
      location.reload();
    }
  });
}

function changeLanguage() {
  if ($("#language-switch").prop("checked") === true) {
    window.location.href = window.location.pathname + "?lng=en";
  } else {
    window.location.href = window.location.pathname + "?lng=vi";
  }
}