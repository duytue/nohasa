$("body").niceScroll({
  cursorwidth: 6
});

$(window).on("load", onLoad);

function onLoad() {
  createChatSession();
  getLanguageStatus();
}

function createChatSession() {
  $.ajax({
    url: "/createChatSession",
  });
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function getLanguageStatus() {
  lang = getCookie("i18next");
  if (lang === "en") {
    $("#language-switch").prop("checked", true);
  } else {
    $("#language-switch").prop("checked", false);
  }
}