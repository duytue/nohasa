$(window).on("load", initChatbox);

function initChatbox() {
    Talk.ready.then(function () {
        var me = new Talk.User({
            id: getCookie("chat_uuid"),
            name: getCookie("chat_uuid"),
            welcomeMessage: "Hey there! How are you? :-)"
        });
        window.talkSession = new Talk.Session({
            appId: "tZSyamVL",
            me: me
        });
        var other = new Talk.User({
            id: "1",
            name: "Nohasa Support",
            email: "support@nohasa.net",
            photoUrl: "https://demo.talkjs.com/img/sebastian.jpg",
            welcomeMessage: "Hey, how can I help?"
        });

        var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other));
        conversation.setParticipant(me);
        conversation.setParticipant(other);
        var popup = talkSession.createPopup(conversation, {
            keepOpen: false
        });
        popup.mount({
            show: false
        });

        var button = document.getElementById("btn-getInTouch");
        button.addEventListener("click", function (event) {
            event.preventDefault();
            popup.show();
        })
    });
}