var createError = require("http-errors");
var express = require("express");
var path = require("path");
var logger = require("morgan"); // default expressjs log
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var cache = require("memory-cache");
var _ = require("lodash");
var compression = require("compression");
var helmet = require("helmet");
var minifyHTML = require("express-minify-html");
var chalk = require("chalk");
var datetime = require("./helper/datetime");
var logFile = require("./helper/logger"); // custom log file
var config = require("./config/config");
var i18next = require("i18next");
var i18nextMiddleware = require("i18next-express-middleware");
var Backend = require("i18next-node-fs-backend");

// routes
var indexRouter = require("./routes/index");
var productRouter = require("./routes/products");

var app = express();
app.set("trust proxy", true);

app.locals._ = _; // use lodash in view

// i18n
i18next
  .use(Backend)
  .use(i18nextMiddleware.LanguageDetector)
  .init({
    backend: {
      loadPath: __dirname + "/locales/{{lng}}/{{ns}}.json",
      addPath: __dirname + "/locales/{{lng}}/{{ns}}.missing.json"
    },
    detection: {
      order: ["querystring", "cookie"],
      caches: ["cookie"]
    },
    fallbackLng: "vi",
    preload: ["vi", "en"],
    saveMissing: true
  });

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(i18nextMiddleware.handle(i18next));

var loggerFormat = "";
if (process.env.NODE_ENV === "production") {
  loggerFormat = function (tokens, req, res) {
    return [
      chalk.blue(req.ip),
      chalk.white(tokens.method(req, res)),
      tokens.url(req, res),
      chalk.green(tokens.status(req, res)),
      tokens.res(req, res, "content-length"),
      "-",
      tokens["response-time"](req, res),
      "ms",
      chalk.yellow(req.headers["user-agent"]),
      " == ",
      datetime.getDateTime()
    ].join(" ");
  };
} else {
  loggerFormat = "dev";
}

app.use(logger(loggerFormat));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false
  })
);
app.use(cookieParser());
app.use(bodyParser.json());
app.use(helmet()); // protect from vulnerabilities
app.use(compression()); // compress route for prod optimizaion
// minify HTML
app.use(
  minifyHTML({
    override: true,
    exception_url: false,
    htmlMinifier: {
      removeComments: true,
      collapseWhitespace: true,
      collapseBooleanAttributes: true,
      removeAttributeQuotes: true,
      removeEmptyAttributes: true,
      minifyJS: true
    }
  })
);

app.use(express.static(path.join(__dirname, "public")));

// caching
let memCache = new cache.Cache();
let cacheMiddleware = duration => {
  return (req, res, next) => {
    let key = "__express__" + req.originalUrl || req.url;
    let cacheContent = memCache.get(key);
    if (cacheContent) {
      res.send(cacheContent);
      return;
    } else {
      res.sendResponse = res.send;
      res.send = body => {
        memCache.put(key, body, duration * 1000);
        res.sendResponse(body);
        logFile.info("Cache is: " + body);
      };
      next();
    }
  };
};

// enforce ssl
if (process.env.NODE_ENV === "production") {
  app.all("*", ensureSecure);
}

function ensureSecure(req, res, next) {
  if (req.secure) {
    return next();
  }
  res.redirect("https://" + req.hostname + req.url);
}

app.use("/", cacheMiddleware(config.application.CACHE_TIMEOUT), indexRouter);
app.use("/products", productRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
  console.log("============ " + err + " ===============");
  logFile.error("============ " + err + " ===============");
});

module.exports = app;