var axios = require("axios");
var config = require("../config/config");
var _ = require("lodash");
var logFile = require("../helper/logger");
var uuid = require("uuid/v4");

// Get all product homepage
function getAllProduct() {

  return axios.get(config.application.API_URL + `getAllProduct`)
    .then(response => {
      return response.data.data;
    }).catch(function (error) {
      logFile.error("getAllProduct: " + error);
    });
}

exports.get_list_product = function (req, res) {
  getAllProduct(req).then((data) => {

    res.render("screens/index", {
      title: "Home Page",
      products: data
    });
  });
};
// end get all product for homepage

// search product
function searchProducts(searchQuery) {

  return axios.get(config.application.API_URL + `search?searchQuery=${searchQuery}`)
    .then(response => {
      return response.data.data;
    }).catch(function (error) {
      logFile.error("searchProducts: " + error);
    });
}

exports.search_product = function (req, res) {
  searchProducts(req.query.search).then((data) => {

    res.render("screens/index", {
      title: "Home Page",
      products: data
    });
  });
};
// end of search

// generate uuid to create ghost user for chat session
exports.create_chat_session = function (req, res) {
  if (req.cookies.chat_uuid === undefined || req.cookies.chat_uuid === "" || req.cookies.chat_uuid === null) {
    res.cookie("chat_uuid", uuid(), {
      expires: new Date(Date.now() + 86400000)
    });
  } else {
    return;
  }
  res.send("create talkjs session success");
};