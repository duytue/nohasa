var axios = require("axios");
var config = require("../config/config");
var _ = require("lodash");
var logFile = require("../helper/logger");

function getProductsByAltName(altName) {
    return axios
        .get(config.application.API_URL + `getAllProductByAltName?altName=${altName}`)
        .then(response => {
            return response.data.data;
        })
        .catch(function (error) {
            logFile.error("getProductsByAltName: " + error);
        });
}

exports.getProductByAltName = function (req, res) {
    getProductsByAltName(req.params.altName).then(data => {
        if (data.singleProduct === undefined) {
            res.render("partials/_404", {
                title: "Product not available"
            });
        } else {
            res.render("screens/products/product-details", {
                productId: data.singleProduct.id,
                title: data.singleProduct.name,
                productName: data.singleProduct.name,
                description: data.singleProduct.description,
                imageURL: data.singleProduct.imageURL,
                price: data.singleProduct.price,
                listImages: data.imageData
            });
        }
    });
};